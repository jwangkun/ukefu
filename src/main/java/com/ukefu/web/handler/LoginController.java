package com.ukefu.webim.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.core.UKDataContext;
import com.ukefu.util.Menu;
import com.ukefu.util.UKTools;
import com.ukefu.webim.service.repository.UserRepository;
import com.ukefu.webim.web.model.User;

/**
 *
 * @author UK
 * @version 1.0.0
 *
 */
@Controller
public class LoginController extends Handler{
	
	@Autowired
	private UserRepository userRepository;

    @RequestMapping(value = "/login" , method=RequestMethod.GET)
    @Menu(type = "apps" , subtype = "login" , access = true)
    public String login(HttpServletRequest request, HttpServletResponse response) {
        return "login";
    }
    
    @RequestMapping(value = "/login" , method=RequestMethod.POST)
    @Menu(type = "apps" , subtype = "login" , access = true)
    public ModelAndView login(HttpServletRequest request, HttpServletResponse response , @Valid User user , @RequestHeader(value = "referer", required = false) final String referer) {
    	ModelAndView view = request(super.createRequestPageTempletResponse("redirect:/"));
    	if(request.getSession(true).getAttribute(UKDataContext.USER_SESSION_NAME) ==null){
	        if(user!=null && user.getUsername()!=null){
		    	User loginUser = userRepository.findByUsernameOrEmail(user.getUsername() , user.getUsername()) ;
		        if(loginUser!=null && loginUser.getPassword().equals(UKTools.md5(user.getPassword()))){
		        	loginUser.setLogin(true);
		        	super.setUser(request, loginUser);
		        	if(!StringUtils.isBlank(referer)){
		        		view = request(super.createRequestPageTempletResponse("redirect:"+referer));
			    	}
		        }else{
		        	view = request(super.createRequestPageTempletResponse("/login"));
		        	if(!StringUtils.isBlank(referer)){
			    		view.addObject("referer", referer) ;
			    	}
		        	view.addObject("msg", "0") ;
		        }
	        }
    	}
		return view;
    }
    
    @RequestMapping("/logout")  
    public String logout(HttpServletRequest request  ){  
    	request.getSession().removeAttribute(UKDataContext.USER_SESSION_NAME) ;
         return "login";
    }  
    
}